module.exports = {
   plugins: [
      {
         resolve: 'gatsby-plugin-manifest',
         options: {
            icon: 'icon.svg'
         }
      },
      'gatsby-plugin-offline'
   ]
}
